# MXNET/EXAMPLE/RCNN Fork

This is sub-folder example/rcnn forked at https://github.com/apache/incubator-mxnet/tree/b5648a43955f7d05c0e53c1ab61a58bd402b4416

## Retrain Plan
1. Replace last FC layer on classification and BBox with 3 outputs: face, DL, HC
2. Find
    - Pre-trained VC07+VC12 model on MXNNet ResNet-101
    - VOC07/12 person images: training set + validation set + test set
    - DL/HC images: training set + validation set + test set
3. Load the model into modified network
4. Freeze all layers except for the last FC layer
5. Train, test and validation

## Quick Info
1.  Cross-validation:
    - Training set (say, 60%), a validation set (e.g. 20%) and a test set (e.g. 20%)
2.  Summarize:
    - Training set  --> to fit the parameters [i.e., weights]
    - Validation set --> to tune the parameters [i.e., architecture]
    - Test set --> to assess the performance [i.e., generalization and predictive power]
3. Common cross-validation methods<br>
In k-fold cross-validation, the original sample is randomly partitioned into k equal sized subsamples. Of the k subsamples, a single subsample is retained as the validation data for testing the model, and the remaining k − 1 subsamples are used as training data. The cross-validation process is then repeated k times (the folds), with each of the k subsamples used exactly once as the validation data. The k results from the folds can then be averaged to produce a single estimation.

## Retrain Techniques
1. Early Stopping<br>
Deep learning methods are slow to train.This often means we cannot use gold standard methods to estimate the performance of the model such as k-fold cross validation.<br>
Early stopping is a type of regularization to curb overfitting of the training data and requires that you monitor the performance of the model on training and a held validation datasets, each epoch.<br>
Once performance on the validation dataset starts to degrade, training can stop.<br>
You can also setup checkpoints to save the model if this condition is met (measuring loss of accuracy), and allow the model to keep learning.<br>
Checkpointing allows you to do early stopping without the stopping, giving you a few models to choose from at the end of a run.<br>

2. Click [here](https://machinelearningmastery.com/improve-deep-learning-performance/) for full article